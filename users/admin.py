from django.contrib import admin

from articles import models as art_models
from . import models


class ArticlesInline(admin.TabularInline):
    model = art_models.Article
    extra = 0


class UserAdmin(admin.ModelAdmin):
    inlines = [ArticlesInline]
    list_display = ('username', 'token')


admin.site.register(models.User, UserAdmin)
