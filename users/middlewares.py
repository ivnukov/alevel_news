from django.contrib import messages
from django.contrib.auth import logout
from django.http import HttpResponseRedirect
from django.urls import reverse
from django.utils import timezone


class EmailConfirmNotifier:
    def __init__(self, get_response):
        self.get_response = get_response

    def __call__(self, request, *args, **kwargs):
        if request.user.is_authenticated and not request.user.email_confirmed:
            messages.add_message(request, messages.WARNING, 'Email is not confirmed!')
            if (timezone.now() - request.user.date_joined).days > 3:
                request.user.deactivate()
                logout(request)
                return HttpResponseRedirect(reverse('homepage'))
        response = self.get_response(request)
        return response
