from datetime import timedelta

from django.conf import settings
from django.core.management import BaseCommand
from django.utils import timezone

from users.models import User


class Command(BaseCommand):
    help = f'Remove users that were deactivated more than {settings.REMOVE_USERS_AFTER} days'

    def add_arguments(self, parser):
        parser.add_argument('days_to_remove', nargs='?', default=settings.REMOVE_USERS_AFTER, type=int)
        parser.add_argument('--user', nargs='?', type=int)

    def handle(self, *args, **options):
        if options.get('user'):
            users = User.objects.filter(id=options.get('user'))
        else:
            users = User.objects.filter(inactive_from__lte=timezone.now() - timedelta(days=options.get('days_to_remove')))
        users.delete()

