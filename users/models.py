import uuid

from django.contrib.auth.models import AbstractUser
from django.db import models
from django.utils import timezone


class User(AbstractUser):
    phone_number = models.CharField(max_length=15, blank=True, null=True)
    token = models.CharField(max_length=32, blank=True, null=True)
    email_confirmed = models.BooleanField(default=False)

    inactive_from = models.DateTimeField(blank=True, null=True)

    def save(self, *args, **kwargs):
        self.email = self.email.lower()
        if not self.token:
            self.token = str(uuid.uuid4()).replace('-', '')
        return super(User, self).save(*args, **kwargs)

    def deactivate(self):
        self.is_active = False
        self.inactive_from = timezone.now()
        self.save()
