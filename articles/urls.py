from django.urls import path

from articles import views

urlpatterns = [
    path('', views.ArticlesListView.as_view(), name='all_articles'),
    path('delete/<int:pk>', views.ArticleDelete.as_view(), name='article_delete'),
    path('create/', views.ArticleCreate.as_view(), name='new_article'),
    path('edit/<int:pk>', views.ArticleUpdate.as_view(), name='edit_article'),
    path('about_us', views.ProtectedTemplateView.as_view(template_name='articles/about.html'), name='about_us'),
    path('category/<int:pk>', views.CategoryDetailView.as_view(), name='category_detail'),
    path('<int:article_id>/like/', views.like_article, name='like_article'),
    path('<int:article_id>', views.article_by_id, name='article_by_id'),
    path('<slug:article_name>', views.article_by_name, name='article_by_name'),
    path('search/', views.search, name='article_search'),
]
