import datetime

from django.contrib.auth.mixins import UserPassesTestMixin
from django.http import JsonResponse
from django.shortcuts import render, redirect
from django.urls import reverse_lazy
from django.views.generic import TemplateView, DetailView, ListView, DeleteView, CreateView, UpdateView

from articles import models, forms


class ProtectedTemplateView(UserPassesTestMixin, TemplateView):
    def test_func(self):
        return self.request.user.is_superuser

    def dispatch(self, request, *args, **kwargs):
        user_test_result = self.get_test_func()()
        if not user_test_result:
            return redirect('/')
        return super().dispatch(request, *args, **kwargs)


def index(request):
    articles = models.Article.objects.filter(is_deleted=False)
    categories = models.Category.objects.filter(parent__isnull=True).order_by('name')
    return render(request, 'articles/base.html',
                  {'categories': categories,
                   'articles': articles})


def article_by_name(request, article_name):
    now = datetime.datetime.now()
    return render(request, 'articles/article_by_name.html',
                  {'name': article_name,
                   'len': len(article_name),
                   'rendered': now})


def article_by_id(request, article_id):
    try:
        article = models.Article.objects.get(id=article_id)
    except (models.Article.DoesNotExist, models.Article.MultipleObjectsReturned):
        return redirect('/')
    return render(request, 'articles/article_by_id.html', {'article': article})


def search(request):
    # Get request value
    # Filter articles
    # Render page
    search = request.GET.get('search', '')
    articles = models.Article.objects.filter(title__icontains=search)
    return render(request, 'articles/search_result.html', {'articles': articles, 'search': search})


def like_article(request, article_id):
    # Dont scare this. It was just complicated example. lol
    article = models.Article.objects.get(id=article_id)
    if request.method == 'POST':
        article.add_like(request.user)
    return JsonResponse(dict(count=article.likes_set.count()))
    # return redirect(reverse('article_by_id', kwargs={"article_id": article_id}))


class CategoryDetailView(DetailView):
    model = models.Category
    context_object_name = 'category'


class ArticlesListView(ListView):
    model = models.Article
    queryset = models.Article.objects.filter(is_deleted=False)
    ordering = '-title'
    paginate_by = 3

    def get_context_data(self, *, object_list=None, **kwargs):
        categories = models.Category.objects.all()
        context = super().get_context_data()
        context.update(dict(categories=categories))
        return context


class ArticleDelete(DeleteView):
    model = models.Article
    success_url = reverse_lazy('all_articles')


class ArticleCreate(CreateView):
    model = models.Article
    form_class = forms.ArticleForm
    success_url = reverse_lazy('all_articles')

    def form_valid(self, form):
        """If the form is valid, save the associated model."""
        self.object = form.save(commit=False)
        self.object.author = self.request.user
        self.object.save()
        return super().form_valid(form)


class ArticleUpdate(UpdateView):
    model = models.Article
    form_class = forms.ArticleForm
    success_url = reverse_lazy('all_articles')
