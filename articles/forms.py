from django.forms import ModelForm

from articles import models


class ArticleForm(ModelForm):
    class Meta:
        model = models.Article
        exclude = ('is_deleted', 'author', 'deleted_at')
